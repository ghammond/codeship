#!/bin/sh

echo 'inside build.sh'
python hello_world.py >  hello_world.stdout
COUNT=$(grep -c 'Hello World!' hello_world.stdout)
if [ $COUNT -gt "0" ]; then
  echo 'success'
  mv hello_world.stdout /tmp
  exit 0
else
  echo 'fail'
  exit 1
fi
